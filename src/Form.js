import React, { Component } from 'react';
import logo from './logo.svg';
import './Forms.css';

class Field extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        value: this.props.value,
        error: false,
      }
      this.onChange = this.onChange.bind(this);
    }
    
    componentWillReceiveProps(update) {
      this.setState({ value: update.value });
    }
    
    onChange(evt) {
      const name = this.props.name;
      const sname = this.props.sname;
      const vnet = this.props.vnet;
      const clprovider = this.props.clprovider;
      const envtype = this.props.envtype;
      const ciname = this.props.ciname;
      const cperson = this.props.cperson;
      const pname = this.props.pname;
      const value = evt.target.value;
      const error = this.props.validate ? this.props.validate(value) : false;
      this.setState({ value, error });
      this.props.onChange({ name, value, error, sname });
    }
    
    render() {
      return (
        <div className={`form-group ${this.state.error && 'has-danger'}`}>
          <input
            className="form-control"
            placeholder={this.props.placeholder}
            value={this.state.value}
            onChange={this.onChange}
          />
          <div className="form-control-feedback">{ this.state.error }</div>
        </div>
      )
    }
    
  }

class Form extends React.Component {
  
    constructor(props) {
      super(props);
      this.onFormSubmit = this.onFormSubmit.bind(this);
      this.onInputChange = this.onInputChange.bind(this);
      this.state = {
        fields: {},
        fieldErrors: {},
        people: []
      }
    }
    
    static isEmail(email) {
      if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){
        return true;
      } else {
        return false
      }  
    }
    
    onInputChange({name, value, error, sname,vnet,clprovider,envtype,envname,ciname,cperson,pname}) {
      const fields = this.state.fields;
      const fieldErrors = this.state.fieldErrors;

      fields[vnet] = value;
      fields[clprovider] = value;
      fields[envtype] = value;
      fields[ciname] = value;
      fields[cperson] = value;
      fields[pname] = value;
      fields[envname] = value;
      fields[sname] = value;
      fields[name] = value;
      fieldErrors[name] = error;
      
      this.setState({fields, fieldErrors });
    }
    
    onFormSubmit(evt) {
      const people = this.state.people;
      const person = this.state.fields;
      evt.preventDefault();
      
      if(this.validate()) return;
      people.push(person);
      this.setState({people, fields: {}});
    }
    
    validate() {
      const person = this.state.fields;
      const fieldErrors = this.state.fieldErrors;
      const errMessages = Object.keys(fieldErrors).filter((k) => fieldErrors[k]);
      if(!person.email) return true;
      if(!person.sname) return true;
      if(!person.envname) return true;
      if(!person.pname) return true;
      if(!person.cperson) return true;
      if(!person.envtype) return true;
      if(!person.clprovider) return true;
      if(!person.vnet) return true;
      if(!person.name) return true;
      if(errMessages.length) return true;
      return false;
    }
    
    render() {
      return (
        <div>
        <form onSubmit={this.onFormSubmit}>
            <Field
                placeholder=' Name'
                name='name'
                value={this.state.fields.name || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : ' Name Required')}
                />
            <Field
                placeholder='Project Name'
                name='pname'
                value={this.state.fields.pname || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Project Name Required')}
                />
            
            <Field
                placeholder='Environment Name'
                name='envname'
                value={this.state.fields.envname || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Env.Name Required')}
                />
            
            <Field
                placeholder='Service Name'
                name='sname'
                value={this.state.fields.sname || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Service Name Required')}
                />

            <Field
                placeholder='Contact Person'
                name='cperson'
                value={this.state.fields.cperson || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Contact Person Required')}
                />

            <Field
                placeholder='Circle Name'
                name='ciname'
                value={this.state.fields.ciname || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Circle Name Required')}
                />

            <Field
                placeholder='Environment Type'
                name='envtype'
                value={this.state.fields.envtype || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Enviroment Type Required')}
                />

            <Field
                placeholder='Cloud Provider'
                name='cprovider'
                value={this.state.fields.cprovider || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Cloud Provider Required')}
                />

            <Field
                placeholder='Vnet'
                name='vnet'
                value={this.state.fields.vnet || ''}
                onChange={this.onInputChange}
                validate={(val) => (val ? false : 'Vnet Required')}
                />

            <Field
                placeholder='Email'
                name='email'
                value={this.state.fields.email || ''}
                onChange={this.onInputChange}
                validate={(val) => (Form.isEmail(val) ? false : 'Invalid Email')}
                />
            
            <input className="btn btn-primary" type="submit" disabled={this.validate()} />
            
          </form>
          <hr />
          <h3>Names</h3>
          <ul>
              { this.state.people.map(({ name, email }, i) =>
                <li key={i}>{name} ({ email })</li>
              ) }
          </ul>
        </div>
      );
    }
  }
  export default Form;