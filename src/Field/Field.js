import React from 'react';
import './Field.css';

const field  = (props) => {
    return (
    <div className="Field">
    <p onClick={props.click}>{props.name}</p>
    <p>{props.children}</p>
    <input type="text" onChange={props.changed}></input>
    </div>
    )
};

export default field;