import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Field from './Field/Field'

class App extends React.Component {
  state = {
    fields: [
      {name: 'max', age:22},
      {name: "juan", age:22}
    ],
    otherState: 'test'
  }
  
  switchNameHandler = (newName)=> {
    this.setState({
      fields:[
        {name: newName, age:22},
        {name: "juan", age:22}
      ]
    })
  }

  nameChangedHandler = (event) => {
    this.setState({
      fields:[
        {name: 'nameChangedHandler', age:22},
        {name: event.target.value, age:22}
      ]
    })
  }
  
  render() {
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border:'1x solid blue',
      padding:'8x',
      cursor:'pointer'
    };
    return (
      <div className="App">
      <h1>Cluster Request Form</h1>
      <h3>Please, fill up the fields below and press 'Send' button</h3>
      <button 
      style={style}
      onClick={this.switchNameHandler.bind(this, 'thisValue')}>Switch Name</button>
      <Field name="Project Name:"/>
      <Field name="Service Name:"/>
      <Field 
        name={this.state.fields[1].name}
        click={this.switchNameHandler.bind(this,'ThisValue2')}
        changed={this.nameChangedHandler}>My hobbies </Field>
      <Field name="Enviroment Type:"/>
      <Field name="Contact Email Name:"/> 
      <Field name={this.switchNameHandler.bind(this, 'Max!')}/>
      <button> Send </button>
      </div> 
      
    );
    //return React.createElement('div',null, React.createElement("h1",null,'Risas'))
  }
}

export default App;
